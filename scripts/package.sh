#!/bin/bash

set -xe

mkdir tmp-package

cp src/translations/*.qm tmp-package/
cp src/libxpiks-helloworld-plugin.so tmp-package/
cp src/helloworld.json tmp-package/

pushd tmp-package
zip -r ../xpiks-helloworld-plugin.xpiksplugin *
popd

rm -r tmp-package