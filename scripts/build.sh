#!/bin/bash

BUILD_MODE=${1:-debug}
QT_ADDITIONAL_CONFIG=

case ${BUILD_MODE} in
    release)
        TARGET="release"
        ;;
    debug)
        TARGET="debug"
        ;;
    gitlab-ci)
        TARGET="debug"
        QT_ADDITIONAL_CONFIG="gitlab-ci"
        ;;
    *)
        echo "$(basename ${0}) error: Invalid command arguments"
        echo "    usage: $(basename ${0}) <debug|release> [optional make flags]"
        exit 1
        ;;
esac

shift
MAKE_FLAGS="-j$(nproc) ${*}"

if [[ -d "$HOME/Qt" ]]; then
    QMAKE="$(find $HOME/Qt/ -type f -name 'qmake')"
    LUPDATE="$(find $HOME/Qt/ -type f -name 'lupdate')"
    LRELEASE="$(find $HOME/Qt/ -type f -name 'lrelease')"
else
    QMAKE=qmake
    LUPDATE=lupdate
    LRELEASE=lrelease
fi

DEFAULT_ROOT_DIR=$(git rev-parse --show-toplevel)
ROOT_DIR=${ROOT_DIR:-$DEFAULT_ROOT_DIR}
cd "${ROOT_DIR}"

set -xe

### build plugin
pushd "${ROOT_DIR}/src"
${QMAKE} "CONFIG+=${TARGET} ${QT_ADDITIONAL_CONFIG}" xpiks-helloworld-plugin.pro
make ${MAKE_FLAGS}
popd

### translations
pushd ${ROOT_DIR}/src/translations/
LUPDATE=$LUPDATE LRELEASE=$LRELEASE make release
echo -e "${PRINT_PREFIX} Generating translations... - done."
popd

echo -e "Build for ${BUILD_MODE}: done."
