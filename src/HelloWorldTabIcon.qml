/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

import QtQuick 2.2
import "qrc:/StyledControls"

Rectangle {
    id: helloWorldIconWrapper
    width: 24
    height: 20
    border.color: isHighlighted ? uiColors.labelActiveForeground : uiColors.inactiveControlColor
    border.width: 2
    color: "transparent"

    StyledText {
        text: "hw"
        color: helloWorldIconWrapper.border.color
        font.pixelSize: 10
        anchors.verticalCenterOffset: 1
        anchors.centerIn: parent
    }
}
