/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef HELLOWORLDWORKER_H
#define HELLOWORLDWORKER_H

#include <memory>

#include <QHash>
#include <QObject>
#include <QString>
#include <QThread>
#include <QtGlobal>

#include <Common/itemprocessingworker.h>

class HelloWorkerCommand;

class HelloWorldWorker: public QThread, public Common::ItemProcessingWorker<HelloWorkerCommand>
{
    Q_OBJECT
    // QThread interface
protected:
    void run() override;

protected:
    virtual bool initWorker() override;
    virtual void processOneItem(const std::shared_ptr<HelloWorkerCommand> &item) override;
    virtual void onWorkerStopped() override { emit stopped(); }

protected:
    virtual void onQueueIsEmpty() override { emit queueIsEmpty(); }

public slots:
    void process() { doWork(); }
    void cancel() { stopWorking(); }

signals:
    void stopped();
    void queueIsEmpty();

private:
    QHash<qint64, int> m_ResultsHash;
};

#endif // HELLOWORLDWORKER_H
