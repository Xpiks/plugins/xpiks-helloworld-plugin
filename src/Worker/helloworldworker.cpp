/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "helloworldworker.h"

#include <QStringList>

#include <Artworks/iartworkmetadata.h>
#include <Common/logging.h>

#include "Worker/helloworkercommand.h"

void HelloWorldWorker::run() {
    doWork();
    reportResults();
}

bool HelloWorldWorker::initWorker() {
    LOG_DEBUG << "#";
    return true;
}

void HelloWorldWorker::processOneItem(const std::shared_ptr<HelloWorkerCommand> &item) {
    auto &checkable = item->getInnerItem();

    int warningsFlags = 0;

    if (checkable->hasKeywords(QStringList() << "test")) {
        // heavy calculation here ...
        warningsFlags = 1 << 30;
        m_ResultsHash.insert(0, warningsFlags);
    } else {
        // return result from hash
        warningsFlags = m_ResultsHash[0];
    }
}
