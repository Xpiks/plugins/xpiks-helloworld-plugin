/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef HELLOWORKERCOMMAND_H
#define HELLOWORKERCOMMAND_H

#include <memory>

#include <Common/flags.h>

namespace Artworks {
    class IArtworkMetadata;
}

class HelloWorkerCommand {
public:
    HelloWorkerCommand(std::shared_ptr<Artworks::IArtworkMetadata> const &basicArtwork,
                       Common::WarningsCheckFlags flags = Common::WarningsCheckFlags::All) :
        m_BasicArtwork(basicArtwork),
        m_CommandFlags(flags)
    {
    }

public:
    std::shared_ptr<Artworks::IArtworkMetadata> const &getInnerItem() const { return m_BasicArtwork; }
    int getFlags() const { return (int)m_CommandFlags; }

private:
    std::shared_ptr<Artworks::IArtworkMetadata> m_BasicArtwork;
    Common::WarningsCheckFlags m_CommandFlags;
};

#endif // HELLOWORKERCOMMAND_H
