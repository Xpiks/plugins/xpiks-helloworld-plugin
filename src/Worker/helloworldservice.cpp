/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "helloworldservice.h"

#include <QCoreApplication>
#include <QDeadlineTimer>
#include <QDebug>
#include <QThread>
#include <QtGlobal>
#include <QTimer>

#include "Worker/helloworkercommand.h"
#include "Worker/helloworldworker.h"

HelloWorldService::HelloWorldService(QObject *parent): QObject(parent) { }

HelloWorldService::~HelloWorldService() {
    LOG_DEBUG << "#";
}

void HelloWorldService::startService() {
    LOG_DEBUG << "#";
    m_Worker.start();
    m_Worker.waitIdle();
}

void HelloWorldService::stopService() {
    LOG_DEBUG << "#";
    m_Worker.stopWorking(true /*immediately*/);
    m_Worker.wait();
}

void HelloWorldService::submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item) {
    Common::WarningsCheckFlags defaultFlags = Common::WarningsCheckFlags::All;
    this->submitItem(item, defaultFlags);
}

void HelloWorldService::submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item,
                                   Common::WarningsCheckFlags flags) {
    if (!m_IsAvailable) { return; }
    auto command = std::make_shared<HelloWorkerCommand>(item, flags);
    m_Worker.submitItem(command);
}
