<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>HelloWorldDialog</name>
    <message>
        <location filename="../HelloWorldDialog.qml" line="96"/>
        <source>Hello World plugin</source>
        <translation>Hello World plugin</translation>
    </message>
    <message>
        <location filename="../HelloWorldDialog.qml" line="111"/>
        <source>Change Model</source>
        <translation>Змінити модель</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Actions/donothingaction.cpp" line="12"/>
        <source>Do nothing</source>
        <translation>Зробити нічого</translation>
    </message>
    <message>
        <location filename="../Actions/makeuserhappyaction.cpp" line="12"/>
        <source>Make user happy</source>
        <translation>Ощасливити користувача</translation>
    </message>
</context>
</TS>
