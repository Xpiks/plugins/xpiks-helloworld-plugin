/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

import QtQuick 2.2
import QtQuick.Controls 1.1
import "qrc:/StyledControls"

Item {
    anchors.fill: parent

    function initializeTab() {
        // is called every time you switch to this tab
        greetingText.font.bold = true
    }

    StyledText {
        id: greetingText
        anchors.centerIn: parent
        text: "Hello World!"
    }
}
