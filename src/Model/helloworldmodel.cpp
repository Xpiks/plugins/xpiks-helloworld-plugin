/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "helloworldmodel.h"

HelloWorldModel::HelloWorldModel(QObject *parent) : QObject(parent)
{
    setGreetingText("Hello World!");
}

