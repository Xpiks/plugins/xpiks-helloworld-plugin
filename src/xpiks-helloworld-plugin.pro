#-------------------------------------------------
#
# Project created by QtCreator 2016-02-07T20:53:32
#
#-------------------------------------------------

QT       -= gui

TARGET = xpiks-helloworld-plugin
TEMPLATE = lib
CONFIG += plugin
CONFIG += c++14

!win32 {
    QMAKE_CXXFLAGS += -std=c++14
}

VERSION = 0.0.1.0
QMAKE_TARGET_PRODUCT = xpiks-helloworld-plugin
QMAKE_TARGET_DESCRIPTION = "Sample implementation of a plugin for Xpiks"
QMAKE_TARGET_COPYRIGHT = "Copyright (C) 2016-2021 Taras Kushnir"

#DEFINES += XPIKSHELLOWORLDPLUGIN_LIBRARY
DEFINES += QT_NO_CAST_TO_ASCII \
           QT_RESTRICTED_CAST_FROM_ASCII \
           QT_NO_CAST_FROM_BYTEARRAY
DEFINES += QT_MESSAGELOGCONTEXT
DEFINES += WITH_PLUGINS

XPIKS_ROOT = $$(XPIKS_ROOT)
isEmpty(XPIKS_ROOT) {
    message("Setting XPIKS_ROOT to default value")
    XPIKS_ROOT=../../..
} else {
    message("Using XPIKS_ROOT from environment")
}

# Add "CONFIG+=separate-tree" when building in Qt Creator
separate-tree {
    XPIKS_ROOT=../../xpiks
}

gitlab-ci {
    XPIKS_ROOT=../vendors/xpiks
}

INCLUDEPATH += $${XPIKS_ROOT}/src/xpiks-corelib/

SOURCES += \
        $${XPIKS_ROOT}/src/xpiks-corelib/Helpers/threadhelpers.cpp \
        $${XPIKS_ROOT}/src/xpiks-corelib/Common/logging.cpp \
        Actions/makeuserhappyaction.cpp \
        Actions/donothingaction.cpp

SOURCES += xpikshelloworldplugin.cpp \
    Worker/helloworldworker.cpp \
    Worker/helloworldservice.cpp \
    Model/helloworldmodel.cpp

HEADERS += xpikshelloworldplugin.h \
    Actions/makeuserhappyaction.h \
    Actions/donothingaction.h \
    Worker/helloworldworker.h \
    Worker/helloworkercommand.h \
    Worker/helloworldservice.h \
    Model/helloworldmodel.h

DISTFILES += \
    helloworld.json

RESOURCES += \
    helloworldresources.qrc

BRANCH_NAME=$$system(git rev-parse --abbrev-ref HEAD)

CONFIG(debug, debug|release)  {
    message("Building debug")
} else {
    message("Building release")
}
