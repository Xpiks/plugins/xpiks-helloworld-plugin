#include "donothingaction.h"

namespace Actions {
    int DoNothingAction::s_ActionID = 456;

    DoNothingAction::DoNothingAction(QObject *parent):
        QObject(parent)
    {
    }

    QString DoNothingAction::getActionName() const {
        return QObject::tr("Do nothing");
    }

    int DoNothingAction::getActionID() const {
        return s_ActionID;
    }
}
