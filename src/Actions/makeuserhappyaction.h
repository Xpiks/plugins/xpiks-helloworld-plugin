/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef MAKEUSERHAPPYACTION
#define MAKEUSERHAPPYACTION

#include <QObject>
#include <QString>

#include <Plugins/ipluginaction.h>

namespace Actions {
    class MakeUserHappyAction:
            public QObject,
            public Plugins::IPluginAction {
        Q_OBJECT
    public:
        MakeUserHappyAction(QObject *parent = nullptr);

        // IPluginAction interface
    public:
        virtual QString getActionName() const override;
        virtual int getActionID() const override;
        virtual ActionContext getContext() const override { return IPluginAction::General; }

    public:
        static int s_ActionID;
    };
}

#endif // MAKEUSERHAPPYACTION

